﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Viewer {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            using (FileStream fs = File.OpenRead(@"H:\KH2fm.yaz0r\dump_kh2\obj\ACTOR_SORA_H.mdlx")) {
                Previewer p = new Previewer();
                p.Read(fs);
                pb.Image = p.Capture();
            }
        }
    }
}
