﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.IO;
using SlimDX.Direct3D9;
using SlimDX;
using vconv122;
using khkh_xldMii.Mx;
using khkh_xldMii;
using khkh_xldMii.Mc;
using khkh_xldMii.V;
using hex04BinTrack;
using khiiMapv;
using System.Diagnostics;
using System.Windows.Forms;

namespace Viewer {
    public class Previewer {
        public Previewer() { }

        PresentParameters PP {
            get {
                PresentParameters o = new PresentParameters();
                o.AutoDepthStencilFormat = Format.D24X8;
                o.BackBufferCount = 1;
                o.BackBufferWidth = 400;
                o.BackBufferHeight = 400;
                o.EnableAutoDepthStencil = true;
                o.SwapEffect = SwapEffect.Discard;
                o.Windowed = true;
                //o.Multisample = MultisampleType.TwoSamples;
                return o;
            }
        }

        Texex2[] tal = null;
        ffMesh ffmesh = null;

        BB bb = new BB();

        public void Read(Stream fs) {
            Mdlxfst mdlx = null;
            {
                foreach (ReadBar.Barent ent in ReadBar.Explode(fs)) {
                    switch (ent.k) {
                        case 7: // timf
                            tal = TIMc.Load(new MemoryStream(ent.bin, false));
                            break;
                        case 4: // m_ex
                            mdlx = new Mdlxfst(new MemoryStream(ent.bin, false));
                            break;
                    }
                }
            }

            foreach (T31 t31 in mdlx.alt31) {
                AxBone[] alaxb = t31.t21.alaxb.ToArray();
                Matrix[] Ma = new Matrix[alaxb.Length];
                {
                    Vector3[] Va = new Vector3[Ma.Length];
                    Quaternion[] Qa = new Quaternion[Ma.Length];
                    for (int x = 0; x < Ma.Length; x++) {
                        Quaternion Qo;
                        Vector3 Vo;
                        AxBone axb = alaxb[x];
                        int parent = axb.parent;
                        if (parent < 0) {
                            Qo = Quaternion.Identity;
                            Vo = Vector3.Zero;
                        }
                        else {
                            Qo = Qa[parent];
                            Vo = Va[parent];
                        }

                        Vector3 Vt = Vector3.TransformCoordinate(new Vector3(axb.x3, axb.y3, axb.z3), Matrix.RotationQuaternion(Qo));
                        Va[x] = Vo + Vt;

                        Quaternion Qt = Quaternion.Identity;
                        if (axb.x2 != 0) Qt *= (Quaternion.RotationAxis(Vector3.UnitX, axb.x2));
                        if (axb.y2 != 0) Qt *= (Quaternion.RotationAxis(Vector3.UnitY, axb.y2));
                        if (axb.z2 != 0) Qt *= (Quaternion.RotationAxis(Vector3.UnitZ, axb.z2));
                        Qa[x] = Qt * Qo;
                    }
                    for (int x = 0; x < Ma.Length; x++) {
                        Matrix M = Matrix.RotationQuaternion(Qa[x]);
                        M *= (Matrix.Translation(Va[x]));
                        Ma[x] = M;
                    }
                }
                List<Body1e> albody1 = new List<Body1e>();
                Matrix Mv = Matrix.Identity;
                foreach (T13vif t13 in t31.al13) {
                    VU1Mem mem = new VU1Mem();
                    int tops = 0x40, top2 = 0x220;
                    new ParseVIF1(mem).Parse(new MemoryStream(t13.bin, false), tops);
                    Body1e b1 = SimaVU1e.Sima(mem, Ma, tops, top2, t13.texi, t13.alaxi, Mv);
                    albody1.Add(b1);
                }

                {
                    ffmesh = new ffMesh();
                    int ci = tal[0].alt.Count;
                    ffmesh.alal3 = new List<ff3>[ci];
                    for (int x = 0; x < ci; x++) ffmesh.alal3[x] = new List<ff3>();
                    if (true) {
                        int svi = 0;
                        int sti = 0;
                        ff1[] alo1 = new ff1[4];
                        int ai = 0;
                        int[] ord = new int[] { 1, 3, 2 };
                        foreach (Body1e b1 in albody1) {
                            for (int x = 0; x < b1.alvi.Length; x++) {
                                ff1 o1 = new ff1(svi + b1.alvi[x], sti + x);
                                alo1[ai] = o1;
                                ai = (ai + 1) & 3;
                                if (b1.alfl[x] == 0x20) {
                                    ff3 o3 = new ff3(b1.t,
                                        alo1[(ai - ord[0]) & 3],
                                        alo1[(ai - ord[2]) & 3],
                                        alo1[(ai - ord[1]) & 3]
                                        );
                                    ffmesh.alal3[b1.t].Add(o3);
                                }
                                else if (b1.alfl[x] == 0x30) {
                                    ff3 o3 = new ff3(b1.t,
                                        alo1[(ai - ord[0]) & 3],
                                        alo1[(ai - ord[1]) & 3],
                                        alo1[(ai - ord[2]) & 3]
                                        );
                                    ffmesh.alal3[b1.t].Add(o3);
                                }
                            }
                            for (int x = 0; x < b1.alvertraw.Length; x++) {
                                if (b1.alalni[x] == null) {
                                    bb.Eat(Vector3.Zero);
                                    ffmesh.alpos.Add(Vector3.Zero);
                                    continue;
                                }
                                if (b1.alalni[x].Length == 1) {
                                    MJ1 mj1 = b1.alalni[x][0];
                                    mj1.factor = 1.0f;
                                    Vector3 vpos = Vector3.TransformCoordinate(VCUt.V4To3(b1.alvertraw[mj1.vertexIndex]), Ma[mj1.matrixIndex]);
                                    bb.Eat(vpos);
                                    ffmesh.alpos.Add(vpos);
                                }
                                else {
                                    Vector3 vpos = Vector3.Zero;
                                    foreach (MJ1 mj1 in b1.alalni[x]) {
                                        vpos += VCUt.V4To3(Vector4.Transform(b1.alvertraw[mj1.vertexIndex], Ma[mj1.matrixIndex]));
                                    }
                                    bb.Eat(vpos);
                                    ffmesh.alpos.Add(vpos);
                                }
                            }
                            for (int x = 0; x < b1.aluv.Length; x++) {
                                Vector2 vst = b1.aluv[x];
                                ffmesh.alst.Add(vst);
                            }
                            svi += b1.alvertraw.Length;
                            sti += b1.aluv.Length;
                        }
                    }
                }
                break;
            }
        }

        class BB {
            public Vector3
                Maxi = new Vector3(float.MinValue, float.MinValue, float.MinValue),
                Mini = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);

            public void Eat(Vector3 v) {
                Maxi.X = Math.Max(Maxi.X, v.X);
                Maxi.Y = Math.Max(Maxi.Y, v.Y);
                Maxi.Z = Math.Max(Maxi.Z, v.Z);

                Mini.X = Math.Min(Mini.X, v.X);
                Mini.Y = Math.Min(Mini.Y, v.Y);
                Mini.Z = Math.Min(Mini.Z, v.Z);
            }
        }

        static float facts = 1f;

        public Bitmap Capture() {
            using (Control win = new Control()) {
                using (Direct3D direct3D = new Direct3D()) {
                    using (Device device = new Device(direct3D, 0, DeviceType.Hardware, win.Handle, CreateFlags.SoftwareVertexProcessing, PP)) {
                        List<Texture> alt = new List<Texture>();
                        foreach (STim t in tal[0].alt) {
                            using (MemoryStream os = new MemoryStream()) {
                                t.pic.Save(os, System.Drawing.Imaging.ImageFormat.Jpeg);
                                os.Position = 0;
                                alt.Add(Texture.FromStream(device, os));
                            }
                        }

                        try {
                            device.BeginScene();
                            {
                                device.Clear(ClearFlags.Target | ClearFlags.ZBuffer, Color.FromArgb(100, Color.FromKnownColor(KnownColor.Control)).ToArgb(), 1.0f, 0);

                                Vector3 bMax = bb.Maxi;
                                Vector3 bMin = bb.Mini;

                                for (int r = 0; r < ffmesh.alal3.Length; r++) {
                                    List<ff3> al3 = ffmesh.alal3[r];
                                    int cntTri = al3.Count;

                                    if (cntTri == 0) continue;

                                    CustomVertex.PositionColoredTextured[] alv = new CustomVertex.PositionColoredTextured[3 * cntTri];
                                    int wri = 0;
                                    for (int x = 0; x < cntTri; x++) {
                                        ff3 o3 = al3[x];
                                        Debug.Assert(o3.al1.Length == 3);
                                        for (int v = 0; v < 3; v++) {
                                            Vector3 pos = ffmesh.alpos[o3.al1[v].vi];
                                            Vector2 st = ffmesh.alst[o3.al1[v].ti];
                                            alv[wri] = new CustomVertex.PositionColoredTextured(pos, -1, st.X, st.Y);
                                            wri++;
                                        }
                                    }

                                    Matrix Mv = Matrix.Identity;

                                    facts = Math.Min(
                                        411 / Math.Abs(bMax.X - bMin.X),
                                        411 / Math.Abs(bMax.Y - bMin.Y)
                                        );

                                    Mv *= Matrix.Translation(
                                        -(bMax.X + bMin.X) / 2,
                                        -(bMax.Y + bMin.Y) / 2,
                                        0
                                        );
                                    Mv *= Matrix.Scaling(facts, facts, facts);

                                    device.SetRenderState(RenderState.Lighting, false);

                                    device.SetTransform(TransformState.Projection, Matrix.OrthoRH(400, 400, -3000, +3000));
                                    device.SetTransform(TransformState.View, Mv);

                                    device.SetTextureStageState(0, TextureStage.ColorOperation, TextureOperation.SelectArg1);
                                    device.SetTextureStageState(0, TextureStage.ColorArg1, TextureArgument.Texture);
                                    device.SetTextureStageState(0, TextureStage.ColorArg2, TextureArgument.Diffuse);

                                    device.SetTexture(0, alt[r]);

                                    device.VertexFormat = CustomVertex.PositionColoredTextured.Format;
                                    device.DrawUserPrimitives<CustomVertex.PositionColoredTextured>(PrimitiveType.TriangleList, cntTri, alv);
                                }
                            }
                            device.EndScene();
                        }
                        finally {
                            foreach (Texture t in alt) t.Dispose();
                        }

                        Surface surf = device.GetBackBuffer(0, 0);
                        using (DataStream os = Surface.ToStream(surf, ImageFileFormat.Jpg)) {
                            return (Bitmap)Bitmap.FromStream(os);
                        }
                    }
                }
            }
        }

        class VCUt {
            public static Vector3 V4To3(Vector4 v) {
                return new Vector3(v.X, v.Y, v.Z);
            }
        }

        class ff1 {
            public int vi, ti;

            public ff1(int vi, int ti) {
                this.vi = vi;
                this.ti = ti;
            }
        }
        class ff3 {
            public ff3(int shaderi, ff1 x, ff1 y, ff1 z) {
                this.shaderi = shaderi;
                this.al1 = new ff1[] { x, y, z };
            }

            public ff1[] al1;
            public int shaderi;
        }
        class ffMesh {
            public List<Vector3> alpos = new List<Vector3>();
            public List<Vector2> alst = new List<Vector2>();
            public List<ff3>[] alal3 = new List<ff3>[0];
        }
    }
}
