﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LibMdlxthumb;

namespace Testlib {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            MyThumb o = new MyThumb();
            o.Load(@"H:\KH2fm.yaz0r\dump_kh2\obj\P_EX110.mdlx", 0);
            IntPtr p;
            o.Extract(out p);
        }
    }
}
