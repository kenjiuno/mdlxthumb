; example2.nsi
;
; This script is based on example1.nsi, but it remember the directory, 
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install example2.nsi into a directory that the user selects,

Unicode true

!define APP "Mdlxthumb"
!define TTL "Mdlx thumbnail for Windows Explorer"

;--------------------------------

; The name of the installer
Name "${TTL}"

; The file to write
OutFile "Setup_Mdlxthumb.exe"

; The default installation directory
InstallDir "$PROGRAMFILES\${APP}"

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\NSIS_${APP}" "Install_Dir"

; Request application privileges for Windows Vista
RequestExecutionLevel admin

XPStyle on

;--------------------------------

; Pages

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "LibMdlxthumb (required)"

  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  SetOutPath "$INSTDIR\x86"
  File "x86\SlimDX.dll"
  File "bin\release\LibMdlxthumb.dll"
  File "bin\release\LibMdlxthumb.dll.config"
  File "bin\release\LibMdlxthumb.pdb"

  SetOutPath "$INSTDIR\x64"
  File "x64\SlimDX.dll"
  File "bin\release\LibMdlxthumb.dll"
  File "bin\release\LibMdlxthumb.dll.config"
  File "bin\release\LibMdlxthumb.pdb"

  SetOutPath $INSTDIR

  ; Write the installation path into the registry
  WriteRegStr HKLM "SOFTWARE\NSIS_${APP}" "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP}" "DisplayName" "${TTL}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP}" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP}" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP}" "NoRepair" 1
  WriteUninstaller "uninstall.exe"
  
SectionEnd

Section /o "Register as x86 (32-bit windows)"
  IfFileExists "$WINDIR\Microsoft.NET\Framework\v2.0.50727\System.dll" 0 NotFound

  ExecWait '"$WINDIR\Microsoft.NET\Framework\v2.0.50727\RegAsm.exe" /codebase "$INSTDIR\x86\LibMdlxthumb.dll"' $0
  DetailPrint "Result: $0"
  Goto End
NotFound:
  MessageBox MB_OK|MB_ICONEXCLAMATION 'You need to install "Microsoft .NET Framework Version 2.0 Redistributable Package (x86)" before install this.'
  # http://www.microsoft.com/downloads/details.aspx?displaylang=en&FamilyID=0856eacb-4362-4b0d-8edd-aab15c5e04f5

End:
SectionEnd

Section "Register as x64 (64-bit windows)"
  IfFileExists "$WINDIR\Microsoft.NET\Framework64\v2.0.50727\System.dll" 0 NotFound

  ExecWait '"$WINDIR\Microsoft.NET\Framework64\v2.0.50727\RegAsm.exe" /codebase "$INSTDIR\x64\LibMdlxthumb.dll"' $0
  DetailPrint "Result: $0"
  Goto End
NotFound:
  MessageBox MB_OK|MB_ICONEXCLAMATION 'You need to install "Microsoft .NET Framework Version 2.0 Redistributable Package (x64)" before install this.'
  # http://www.microsoft.com/downloads/details.aspx?familyid=B44A0000-ACF8-4FA1-AFFB-40E78D788B00&displaylang=en

End:
SectionEnd

Section "Registry: ShellEx"
  ReadRegStr $0 HKCR ".mdlx" ""
  StrCmp $0 "" NoMdlx

  WriteRegStr HKCR "$0\ShellEx\{BB2E617C-0920-11d1-9A0B-00C04FC2D6C1}" "" "{ed419056-4d9c-41ab-809d-708332cfb331}"
  Goto End
NoMdlx:
  WriteRegStr HKCR ".mdlx" "" "mdlx_auto_file"
  WriteRegStr HKCR "mdlx_auto_file" "" ""
  WriteRegStr HKCR "mdlx_auto_file\ShellEx\{BB2E617C-0920-11d1-9A0B-00C04FC2D6C1}" "" "{ed419056-4d9c-41ab-809d-708332cfb331}"
End:
SectionEnd

Section "Registry: Approved"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Shell Extensions\Approved" "{ed419056-4d9c-41ab-809d-708332cfb331}" "LibMdlxthumb"
SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"

  ExecWait '"$WINDIR\Microsoft.NET\Framework\v2.0.50727\RegAsm.exe" /un "$INSTDIR\LibMdlxthumb.dll"'
  ExecWait '"$WINDIR\Microsoft.NET\Framework64\v2.0.50727\RegAsm.exe" /un "$INSTDIR\LibMdlxthumb.dll"'

  DeleteRegValue HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Shell Extensions\Approved" "{ed419056-4d9c-41ab-809d-708332cfb331}"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP}"
  DeleteRegKey HKLM "SOFTWARE\NSIS_${APP}"

  ; Remove files and uninstaller
  Delete "$INSTDIR\x86\LibMdlxthumb.*"
  Delete "$INSTDIR\x86\SlimDX.*"
  Delete "$INSTDIR\x64\LibMdlxthumb.*"
  Delete "$INSTDIR\x64\SlimDX.*"

  Delete "$INSTDIR\uninstall.exe"

  ; Remove shortcuts, if any
  #Delete "$SMPROGRAMS\Example2\*.*"

  ; Remove directories used
  #RMDir "$SMPROGRAMS\Example2"
  RMDir "$INSTDIR"

SectionEnd
