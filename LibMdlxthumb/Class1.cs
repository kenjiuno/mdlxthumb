﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.InteropServices;
using System.IO;
using Viewer;
using System.Drawing;
using System.Windows.Forms;
using System.Globalization;
using System.Diagnostics;
using System.Threading;
using System.IO.IsolatedStorage;
using System.Drawing.Imaging;

namespace LibMdlxthumb {
    // CLSID: ed419056-4d9c-41ab-809d-708332cfb331
    [ComVisible(true), ClassInterface(ClassInterfaceType.None), GuidAttribute("ed419056-4d9c-41ab-809d-708332cfb331")]
    public class MyThumb : IPersistFile, IExtractImage {
        #region IPersistFile メンバ

        public void GetClassID(out Guid pClassID) {
            pClassID = new Guid("ed419056-4d9c-41ab-809d-708332cfb331");
        }

        String fp = null;

        public void GetCurFile(out string ppszFileName) {
            ppszFileName = fp;
        }

        public int IsDirty() {
            return 0;
        }

        public void Load(string pszFileName, int dwMode) {
            if (0 != (dwMode & (0x00001000 | 0x00020000 | 0x04000000)))
                throw new InvalidOperationException();

            if (!File.Exists(pszFileName))
                throw new FileNotFoundException();

            fp = pszFileName;
        }

        public void Save(string pszFileName, bool fRemember) {
            throw new NotImplementedException();
        }

        public void SaveCompleted(string pszFileName) {
            throw new NotImplementedException();
        }

        #endregion

        #region IExtractImage メンバ

        public void GetLocation(IntPtr pszPathBuffer, int cch, ref int pdwPriority, ref SIZE prgSize, int dwRecClrDepth, ref int pdwFlags) {
            byte[] bin = Encoding.Unicode.GetBytes(fp + "\0");
            if (cch < bin.Length) throw new ArgumentException();

            Marshal.Copy(bin, 0, pszPathBuffer, bin.Length);

            desired = prgSize;
        }

        SIZE desired = new SIZE(256, 256);

        class Hash4CachedFile {
            public static string Compute(String fpfrm) {
                FileInfo fi = new FileInfo(fpfrm);
                return Path.GetFileNameWithoutExtension(fpfrm) + "!" + fi.Length + "!" + fi.LastWriteTimeUtc.ToBinary() + ".jpg";
            }
        }

        class Extractor {
            public Extractor(String fp) {
                this.fp = fp;
            }

            String fp;
            Bitmap pic = null;
            Exception err = null;

            void Run() {
                try {
                    Thread.CurrentThread.Priority = ThreadPriority.Lowest;

                    Previewer p = new Previewer();
                    using (FileStream fs = File.OpenRead(fp)) {
                        p.Read(fs);
                        pic = p.Capture();
                    }
                }
                catch (Exception err) {
                    this.err = err;
                }
            }

            public static Bitmap Extract(String fp) {
                Extractor o;
                Thread t = new Thread((o = new Extractor(fp)).Run);
                t.Start();
                t.Join();
                if (o.err != null) throw new ApplicationException("Extraction failed!", o.err);
                return o.pic;
            }
        }

        public void Extract(out IntPtr phBmpThumbnail) {
            try {
                Debug.WriteLine("# " + fp + ": Extract");

                using (Bitmap pic = Extractor.Extract(fp)) {
                    phBmpThumbnail = pic.GetHbitmap();
                }

                Debug.WriteLine("# " + fp + ": Done");
            }
            catch (Exception err) {
                using (Bitmap pic = new Bitmap(desired.cx, desired.cy)) {
                    using (Graphics cv = Graphics.FromImage(pic)) {
                        using (Font font = new Font(FontFamily.GenericMonospace, 9)) {
                            StringFormat sf = new StringFormat();
                            cv.Clear(Color.Black);
                            cv.DrawString(err.ToString(), font, Brushes.WhiteSmoke, RectangleF.FromLTRB(0, 0, desired.cx, desired.cy), sf);
                            phBmpThumbnail = pic.GetHbitmap();
                        }
                    }
                }
            }
        }

        #endregion

    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SIZE {
        public int cx, cy;

        public SIZE(int cx, int cy) {
            this.cx = cx;
            this.cy = cy;
        }

        public override string ToString() {
            return String.Format("{0}, {1}", cx, cy);
        }
    }

    [ComImportAttribute()]
    [GuidAttribute("BB2E617C-0920-11d1-9A0B-00C04FC2D6C1")]
    [InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
    interface IExtractImage {
        void GetLocation(
            IntPtr pszPathBuffer,
            int cch,
            ref int pdwPriority,
            ref SIZE prgSize,
            int dwRecClrDepth,
            ref int pdwFlags);

        void Extract(
            out IntPtr phBmpThumbnail);
    }
}
